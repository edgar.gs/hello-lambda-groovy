package com.gs.micronaut

HelloResponse helloLambdaGroovy(HelloRequest req) {
    new HelloResponse(name: "Hello ${req.name}!", ok: true)
}

class HelloRequest implements Serializable{
    String name
}

class HelloResponse extends HelloRequest implements Serializable{
    boolean ok
}