package com.gs.micronaut

import io.micronaut.function.client.FunctionClient
import io.reactivex.Single

import javax.inject.Named

@FunctionClient
interface HelloLambdaGroovyClient {

    @Named("hello-lambda-groovy")
    Single<HelloResponse> helloLambdaGroovy(HelloRequest req)
}
