package com.gs.micronaut

import io.micronaut.context.ApplicationContext
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.Specification
import javax.inject.Inject

@MicronautTest
class HelloLambdaGroovyFunctionSpec extends Specification {

    @Inject
    HelloLambdaGroovyClient client


    void "test hello-lambda-groovy function"() {
        given:
            def req = new HelloRequest(name: "Edgar")
        expect:
            client.helloLambdaGroovy(req).blockingGet().getName() == "Hello Edgar!"
    }
}
